package genetics;

import exceptions.PopulationException;
import map.Route;

public abstract class BaseCrossover {
	public abstract Route cross(Route p1, Route p2) throws PopulationException;
}
