package genetics;

import map.Route;

public abstract class BaseMutation {
	/**
	 * Mutates route passed as argument.
	 * @param route
	 * @return Mutated route.
	 */
	public abstract Route mutate(Route route);
}
