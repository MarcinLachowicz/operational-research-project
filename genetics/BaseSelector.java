package genetics;

public abstract class BaseSelector {
	abstract Population selectPopulation(Population basePopulation);
}
