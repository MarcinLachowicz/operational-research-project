package genetics;

import exceptions.PopulationException;
import map.Map;
import map.Route;

public class GeneticsEngine implements Runnable {
	private int maxIterations, currentIteration, populationSize;
	private Map map;
	private BaseCrossover crossover;
	private BaseMutation mutation;
	private BaseSelector selector;
	private double crossoverProbability, mutationProbability;
	private boolean isWorking = false;

	private Result result = new Result();
	private int stopCond = -1;

	public GeneticsEngine(int maxIterations, int stopCond, int populationSize,
			Map map, BaseSelector selector, BaseCrossover crossover,
			BaseMutation mutation, double corssoverProbability,
			double mutationProbability) {
		this.maxIterations = maxIterations;
		this.populationSize = populationSize;
		this.map = map;
		this.selector = selector;
		this.crossover = crossover;
		this.mutation = mutation;
		this.crossoverProbability = corssoverProbability;
		this.mutationProbability = mutationProbability;
		this.stopCond = stopCond;
	}

	public void run() {
		isWorking = true;
		try {
			Population population = createRandomPopulation(populationSize);
			for (int i = 0; i < maxIterations && isWorking; i++) {
				Population selectedPopulation = selector
						.selectPopulation(population);
				population.clear();
				Population crossoveredPopulation = crossover(selectedPopulation);
				selectedPopulation.clear();
				Population mutatedPopulation = mutation(crossoveredPopulation);
				crossoveredPopulation.clear();
				population = mutatedPopulation;
				Route theBestSpecimen = population.getTheBestSpecimen();
				result.add(new IterationResult(currentIteration, population
						.getTheBestAdaptation(), population
						.getTheWorstAdaptation(), population
						.getAverageAdaptation(), theBestSpecimen));

				currentIteration = i;

				if (isStopCondSet()) {
					isEnd(theBestSpecimen);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			isWorking = false;
		}
	}

	public boolean isStopCondSet() {
		return (stopCond > -1);
	}

	private void isEnd(Route theBestSpecimen) {
		if (map.getDistance(theBestSpecimen) <= stopCond) {
			isWorking = false;
		}
	}

	public Population createRandomPopulation(int size) {
		Population population = new Population(map);
		for (int i = 0; i < size; i++) {
			population.add(map.getRandomRoute());
		}
		return population;
	}

	private Population crossover(Population basePopulation)
			throws PopulationException {
		Population result = new Population(map);

		int size = basePopulation.getSize();
		Route parentHolder = null;
		for (int i = 0; i < size; i++) {
			if (crossoverProbability >= Math.random()) {
				if (parentHolder == null) {
					parentHolder = basePopulation.getSpecimenAt(i);
				} else {
					Route secondParent = basePopulation.getSpecimenAt(i);

					result.add(crossover.cross(parentHolder, secondParent));
					result.add(crossover.cross(secondParent, parentHolder));
					parentHolder = null;
					secondParent = null;
				}
			} else {
				result.add(basePopulation.getSpecimenAt(i));
			}
		}

		if (parentHolder != null) {
			result.add(parentHolder);
		}

		return result;
	}

	private Population mutation(Population basePopulation) {
		Population result = new Population(map);

		int size = basePopulation.getSize();
		for (int i = 0; i < size; i++) {
			if (mutationProbability >= Math.random()) {
				Route specimen = basePopulation.getSpecimenAt(i);
				result.add(mutation.mutate(specimen));
				specimen = null;
			} else {
				result.add(basePopulation.getSpecimenAt(i));
			}
		}

		return result;
	}

	public Population getPopulation() {
		return null;
	}

	public int getCurrentIteration() {
		return currentIteration;
	}

	public int getMaxIterations() {
		return maxIterations;
	}

	public boolean isWorking() {
		return isWorking;
	}

	public Result getResult() {
		return result;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public Map getMap() {
		return map;
	}

	public BaseCrossover getCrossover() {
		return crossover;
	}

	public BaseMutation getMutation() {
		return mutation;
	}

	public BaseSelector getSelector() {
		return selector;
	}

	public double getCrossoverProbability() {
		return crossoverProbability;
	}

	public double getMutationProbability() {
		return mutationProbability;
	}
}
