package genetics;

import java.util.Random;

import map.Route;

public class InversionMutation extends BaseMutation {

	@Override
	public Route mutate(Route route) {
		int length = route.getLength();
		int[] tempResult = new int[length];

		Random rnd = new Random();
		int from = rnd.nextInt(length);
		int to = rnd.nextInt(length);
		if (to < from) {
			int temp = from;
			from = to;
			to = temp;
		}

		for (int i = 0; i < length; i++) {
			if (i >= from && i <= to) {
				tempResult[i] = route.getVerticeIDAt(to - (i - from));
			} else {
				tempResult[i] = route.getVerticeIDAt(i);
			}
		}
		Route result = new Route(tempResult);
		tempResult = null;
		return result;
	}

	public static void main(String[] args) {
		Route temp = new Route(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		InversionMutation mutation = new InversionMutation();
		
		Route mutatedRoute = mutation.mutate(temp);
		System.out.println(mutatedRoute);
	}

}
