package genetics;

import map.Route;

public class IterationResult {
	private int iteration;
	private int min, max;
	private double avg;
	private Route theBestSpecimen;

	public IterationResult(int iteration, int min, int max, double avg, Route theBestSpecimen) {
		this.iteration = iteration;
		this.min = min;
		this.max = max;
		this.avg = avg;
		this.theBestSpecimen = theBestSpecimen;
	}

	public int getIteration() {
		return iteration;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public double getAvg() {
		return avg;
	}

	public Route getTheBestSpecimen() {
		return theBestSpecimen;
	}

	public void setTheBestSpecimen(Route theBestSpecimen) {
		this.theBestSpecimen = theBestSpecimen;
	}
}
