package genetics;

import java.util.Arrays;
import java.util.Random;

import map.Route;
import exceptions.PopulationException;

public class OrderCrossover extends BaseCrossover {

	@Override
	public Route cross(Route p1, Route p2) throws PopulationException {
		Route result = null;
		int length = p1.getLength();

		if (length != p2.getLength() || length == 0) {
			throw new PopulationException(
					"Parents are from diffrent populations");
		} else {
			result = orderCrossover(p1, p2);
		}

		return result;
	}

	private Route orderCrossover(Route p1, Route p2) {
		int[] tempResult;
		int length = p1.getLength();

		Random rnd = new Random();
		int from = rnd.nextInt(length);
		int to = rnd.nextInt(length);
		if (to < from) {
			int temp = from;
			from = to;
			to = temp;
		}

		from = 3;
		to = 7;

		// Create temp result
		tempResult = new int[length];
		Arrays.fill(tempResult, -1);

		// Copy segment from parent1 to child
		for (int i = from; i <= to; i++) {
			tempResult[i] = p1.getVerticeIDAt(i);
		}

		int sourceIndex = to % length;
		for (int i = 0; i < length - to + from; i++) {
			int currentTargetIndex = (to + i) % length;
			while (isInArray(tempResult, p2.getVerticeIDAt(sourceIndex))) {
				sourceIndex++;
				sourceIndex = sourceIndex % length;
			}
			tempResult[currentTargetIndex] = p2.getVerticeIDAt(sourceIndex);
			sourceIndex++;
			sourceIndex = sourceIndex % length;
		}

		return new Route(tempResult);
	}

	private boolean isInArray(int[] array, int key) {
		for (int i : array) {
			if (i == key)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Route p1 = new Route(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		Route p2 = new Route(new int[] { 9, 3, 7, 8, 2, 6, 5, 1, 4 });
		OrderCrossover testObject = new OrderCrossover();

		System.out.println("{1, 2, 3, 4, 5, 6, 7, 8, 9}");
		System.out.println("{9, 3, 7, 8, 2, 6, 5, 1, 4}");
		try {
			Route result = testObject.cross(p1, p2);
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
