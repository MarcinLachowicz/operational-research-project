package genetics;

import static org.junit.Assert.*;
import map.Route;

import org.junit.Test;

import exceptions.PopulationException;

public class OrderCrossoverTest {

	Route p1 = new Route(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
	Route p2 = new Route(new int[] { 9, 3, 7, 8, 2, 6, 5, 1, 4 });
	Route p3 = new Route(new int[] { 1, 2, 3 });
	Route p4 = new Route(new int[] {});
	OrderCrossover testObject = new OrderCrossover();

	@Test(expected = PopulationException.class)
	public void RoutesLengthShouldBeTheSame() throws PopulationException {
		testObject.cross(p1, p3);
	}
	
	@Test(expected = PopulationException.class)
	public void RouteShouldNotBeEmpty() throws PopulationException {
		testObject.cross(p4, p4);
	}

	@Test
	public void testCross() throws PopulationException{
		assertTrue(testObject.cross(p1, p2).equals(new Route(new int[] {3, 8, 2, 4, 5, 6, 7, 1, 9})));
		
	}

}
