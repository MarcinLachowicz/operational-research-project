package genetics;

import java.util.Arrays;
import java.util.Random;

import exceptions.PopulationException;
import map.Route;

public class PartiallyMatchedCrossover extends BaseCrossover {

	@Override
	public Route cross(Route p1, Route p2) throws PopulationException {
		Route result = null;
		int length = p1.getLength();

		if (length != p2.getLength() || length == 0) {
			throw new PopulationException(
					"Parents are from diffrent populations");
		} else {
			result = localCross(p1, p2);
		}

		return result;
	}

	/**
	 * Cross parent1 with parent2.
	 * 
	 * @param p1
	 *            - parent1
	 * @param p2
	 *            - parent2
	 * @return child
	 */
	private Route localCross(Route p1, Route p2) throws PopulationException {
		Route result = null;
		int[] tempResult;

		int length = p1.getLength();

		Random rnd = new Random();
		int from = rnd.nextInt(length);
		int to = rnd.nextInt(length);
		if (to < from) {
			int temp = from;
			from = to;
			to = temp;
		}

		// Create tem result
		tempResult = new int[length];
		Arrays.fill(tempResult, -1);

		// Copy segment from parent1 to child
		for (int i = from; i <= to; i++) {
			tempResult[i] = p1.getVerticeIDAt(i);
		}

		// Place non-used vertices from parent2
		for (int i = from; i <= to; i++) {
			int currentVerticeID = p2.getVerticeIDAt(i);
			if (!p1.isVerticeIDBetween(currentVerticeID, from, to)) {
				int oppositeVerticeID = p1.getVerticeIDAt(i);
				int optionsCount = to - from + 1;

				while (p2.isVerticeIDBetween(oppositeVerticeID, from, to)
						&& optionsCount > 0) {
					optionsCount--;
					oppositeVerticeID = p1.getVerticeIDAt(p2
							.getIndexOfVerticeID(oppositeVerticeID));
				}
				if (optionsCount >= 0) {
					tempResult[p2.getIndexOfVerticeID(oppositeVerticeID)] = currentVerticeID;
				}
			}
		}

		for (int i = 0; i < tempResult.length; i++) {
			if (tempResult[i] == -1) {
				tempResult[i] = p2.getVerticeIDAt(i);
			}
		}

		result = new Route(tempResult);
		tempResult = null;
		return result;
	}

	public static void main(String[] args) {
		Route p1 = new Route(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		Route p2 = new Route(new int[] { 9, 3, 7, 8, 2, 6, 5, 1, 4 });
		PartiallyMatchedCrossover c = new PartiallyMatchedCrossover();

		try {
			Route result = c.cross(p1, p2);
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
