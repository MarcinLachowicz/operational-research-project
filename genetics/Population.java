package genetics;

import java.util.LinkedList;

import map.Map;
import map.Route;

public class Population {
	private LinkedList<Route> specimens = new LinkedList<>();
	private LinkedList<Integer> adaptations = new LinkedList<>();
	private Map map;

	public Population(Map map) {
		this.map = map;
	}

	public Route getSpecimenAt(int i) {
		return specimens.get(i);
	}

	public int getSpecimensAdaptationAt(int i) {
		return adaptations.get(i);
	}

	public void add(Route specimen) {
		specimens.add(specimen);
		Integer i = map.getDistance(specimen);
		adaptations.add(i);
	}

	public int getTheWorstAdaptation() {
		int max = 0;
		for (Integer i : adaptations) {
			if (i > max) {
				max = i;
			}
		}
		return max;
	}

	public int getTheBestAdaptation() {
		int min = -1;
		if (getSize() > 0) {
			min = getSpecimensAdaptationAt(0);
			for (Integer i : adaptations) {
				if (i < min) {
					min = i;
				}
			}
		}
		return min;
	}
	
	public Route getTheBestSpecimen() {
		int theBestAdaptation = getTheBestAdaptation();
		Route result = null;
		for(int i=0; i<specimens.size(); i++) {
			if(adaptations.get(i) == theBestAdaptation) {
				result = specimens.get(i);
				break;
			}
		}
		return result;
	}

	public double getAverageAdaptation() {
		double result = 0;
		int size = getSize();

		for (Integer i : adaptations) {
			result += ((double) i) / size;
		}
		return result;
	}

	public Map getMap() {
		return map;
	}

	public int getSize() {
		return specimens.size();
	}

	public void clear() {
		specimens.clear();
		adaptations.clear();
	}

	@Override
	public String toString() {
		String result = new String();
		for (Route r : specimens) {
			result = result + r + "\n";
		}
		return result;
	}
}
