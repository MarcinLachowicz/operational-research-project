package genetics;

import java.util.LinkedList;

public class Result {
	private LinkedList<IterationResult> list = new LinkedList<>();

	public void add(IterationResult ir) {
		list.add(ir);
	}

	public IterationResult get(int i) {
		return list.get(i);
	}

	public int size() {
		return list.size();
	}
}
