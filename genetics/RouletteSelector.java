package genetics;

public class RouletteSelector extends BaseSelector {

	@Override
	Population selectPopulation(Population basePopulation) {
		Population result = new Population(basePopulation.getMap());
		double[] probability = getProbability(basePopulation);

		for (int i = 0; i < basePopulation.getSize(); i++) {
			result.add(basePopulation.getSpecimenAt(getIndexByPercent(
					probability, Math.random())));
		}

		return result;
	}

	private double getDenominator(Population population) {
		double result = 0;
		double theWorstAdaptation = population.getTheWorstAdaptation();

		int size = population.getSize();
		for (int i = 0; i < size; i++) {
			result += (theWorstAdaptation
					- population.getSpecimensAdaptationAt(i) + 1);
		}

		return result;
	}

	private double[] getProbability(Population population) {
		double denominator = getDenominator(population);
		int theWorstAdaptation = population.getTheWorstAdaptation();

		int size = population.getSize();
		double[] result = new double[size];

		for (int i = 0; i < size; i++) {
			result[i] = ((theWorstAdaptation
					- population.getSpecimensAdaptationAt(i) + 1))
					/ (denominator);
		}

		return result;
	}

	private int getIndexByPercent(double[] probability, double pro) {
		int i = 0;
		double temp = 0;

		while (temp <= pro) {
			temp += probability[i++];
		}

		return i-1;
	}

}
