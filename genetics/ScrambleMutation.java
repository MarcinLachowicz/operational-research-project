package genetics;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import map.Route;

public class ScrambleMutation extends BaseMutation {

	@Override
	public Route mutate(Route route) {
		int length = route.getLength();
		int amount = (int) Math.ceil(length / 4.0);
		if (amount % 2 != 0)
			amount++;
		int result[] = route.getVerticesID().clone();
		
		Random rand = new Random();
		Set<Integer> indexSet = new HashSet<Integer>();
		while (indexSet.size() < amount) {
			indexSet.add(rand.nextInt(length));
		}
		
		int[] indexArray = new int[indexSet.size()];
		int index = 0;
		for(Integer i : indexSet){
		    indexArray[index++] = i;
		}
		
//		for(int i=0; i < amount-1; i++){
//			swap(result, indexArray[i], indexArray[i+1]);
//		}
	
		for(int i=0; i<amount; i= i+2){
			swap(result, indexArray[i], indexArray[i+1]);
		}

		return new Route(result);
	}

	  private static void swap(int[] array, int i, int j) {
	        int temp = array[i];
	        array[i] = array[j];
	        array[j] = temp;
	    }
	
	public static void main(String[] args) {
		Route testObject = new Route(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		System.out.println(testObject);
		ScrambleMutation sm = new ScrambleMutation();
		
		System.out.println(sm.mutate(testObject));
	}

}
