package genetics;

import java.util.LinkedList;

import map.Map;
import map.Route;

public class TournamentSelector extends BaseSelector {

	private int groupSize;

	public TournamentSelector(int groupSize) {
		this.groupSize = groupSize;
	}

	@Override
	Population selectPopulation(Population basePopulation) {
		Population result = new Population(basePopulation.getMap());
		for (int i = 0; i < basePopulation.getSize(); i++) {
			result.add(getTheBestRoute(basePopulation.getMap(),
					createGroup(basePopulation, groupSize)));
		}
		return result;
	}

	private LinkedList<Route> createGroup(Population basePopulation,
			int groupSize) {
		LinkedList<Route> group = new LinkedList<>();
		for (int j = 0; j < groupSize; j++) {
			group.add(getRandomRoute(basePopulation));
		}
		return group;
	}

	private Route getTheBestRoute(Map map, LinkedList<Route> group) {
		Route theBest = group.getFirst();
		for (Route r : group) {
			if (map.getDistance(r) < map.getDistance(theBest)) {
				theBest = r;
			}
		}
		return theBest;
	}

	private Route getRandomRoute(Population population) {
		return population.getSpecimenAt((int) (Math.random() * population
				.getSize()));
	}

}
