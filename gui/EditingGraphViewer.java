package gui;

import org.apache.commons.collections15.Factory;

public class EditingGraphViewer {
	private Factory<Integer> vFactory;
	private Factory<Integer> eFactory;
	
	int vCount, eCount;
	
	public EditingGraphViewer() {
		vFactory = new Factory<Integer>() {

			@Override
			public Integer create() {
				return vCount++;
			}
			
		};
		
		eFactory = new Factory<Integer>() {

			@Override
			public Integer create() {
				return eCount++;
			}
		};
	}
	
	public Factory<Integer> getvFactory() {
		return vFactory;
	}
	
	public Factory<Integer> geteFactory() {
		return eFactory;
	}
	
}
