package gui;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JPanel;

import map.Map;
import map.Vertice;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;

public class GraphEditorPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Graph<Integer, Integer> graph;
	private EditingGraphViewer sgv;
	private VisualizationViewer<Integer, Integer> vs;
	private EditingModalGraphMouse<Integer, Integer> gm;
	private AbstractLayout<Integer, Integer> layout;

	public GraphEditorPanel() {
		setBounds(0, 0, 500, 500);
		createGraphView();
		addGraphMouseController();
	}

	private void addGraphMouseController() {
		gm = new EditingModalGraphMouse<Integer, Integer>(
				vs.getRenderContext(), sgv.getvFactory(), sgv.geteFactory());
		vs.setGraphMouse(gm);
	}

	private void createGraphView() {
		graph = new DirectedSparseGraph<>();
		sgv = new EditingGraphViewer();
		layout = new StaticLayout<>(graph);
		vs = new VisualizationViewer<>(layout, getSize());
		vs.getRenderContext().setVertexLabelTransformer(
				new Transformer<Integer, String>() {

					@Override
					public String transform(Integer arg0) {
						return "" + arg0;
					}
				});
		add(vs);
	}

	public EditingModalGraphMouse<Integer, Integer> getGM() {
		return gm;
	}

	public Map getMap() {
		Map result = null;
		Collection<Integer> vertices = graph.getVertices();
		LinkedList<Vertice> mapVertices = new LinkedList<>();
		for (Integer v : vertices) {
			mapVertices.add(new Vertice(v, (int) layout.getX(v), (int) layout
					.getY(v)));
		}
		
		result = new Map("", mapVertices);

		return result;
	}

}
