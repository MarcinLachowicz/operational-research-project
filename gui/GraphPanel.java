package gui;

import java.awt.geom.Point2D;
import javax.swing.JPanel;

import map.Map;
import map.Route;
import map.Vertice;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationImageServer;

public class GraphPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Graph<Vertice, Integer> graph;
	private Transformer<Vertice, Point2D> positionTransformer;
	private Map map = null;
	private VisualizationImageServer<Vertice, Integer> vs;
	private AbstractLayout<Vertice, Integer> layout;

	public GraphPanel() {
		positionTransformer = createPositionTransformer();
	}

	public void loadMap(Map map) {
		this.map = map;
		graph = createGraph();

		layout = new StaticLayout<>(graph, positionTransformer);
		vs = new VisualizationImageServer<>(layout, getSize());
		vs.getRenderContext().setVertexLabelTransformer(
				createLabelTransformer());
		add(vs);
	}

	private Graph<Vertice, Integer> createGraph() {
		Graph<Vertice, Integer> graph = new DirectedSparseGraph<>();

		for (int i = 0; i < map.getSize(); i++) {
			graph.addVertex(map.getVerticeAt(i));
		}
		return graph;
	}

	public void applyPath(Route route) {
		//removeAllEdges();
		Graph<Vertice, Integer> tempGraph = createGraph();
		int length = route.getLength();

		for (int i = 0; i < length - 1; i++) {
			tempGraph.addEdge(i + 1, map.getVerticeById(route.getVerticeIDAt(i)),
					map.getVerticeById(route.getVerticeIDAt(i + 1)));
		}
		tempGraph.addEdge(length,
				map.getVerticeById(route.getVerticeIDAt(length - 1)),
				map.getVerticeById(route.getVerticeIDAt(0)));
		layout.setGraph(tempGraph);
		vs.setGraphLayout(layout);
	}

	public static Transformer<Vertice, Point2D> createPositionTransformer() {
		Transformer<Vertice, Point2D> initializer = new Transformer<Vertice, Point2D>() {

			@Override
			public Point2D transform(Vertice arg0) {
				return new Point2D.Float(arg0.getX(), arg0.getY());
			}
		};
		return initializer;
	}

	public static Transformer<Vertice, String> createLabelTransformer() {
		Transformer<Vertice, String> initializer = new Transformer<Vertice, String>() {
			@Override
			public String transform(Vertice arg0) {
				return "" + arg0.getId();
			}
		};

		return initializer;
	}
}
