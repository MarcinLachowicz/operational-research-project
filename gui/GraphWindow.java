package gui;

import javax.swing.JFrame;

import map.Map;
import map.Route;

import javax.swing.JLabel;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;

public class GraphWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GraphPanel panel;
	private Map map;
	private JLabel labelPath;
	private JLabel labelDistance;
	private JLabel labelIteration;

	public GraphWindow(Map map) {
		setBounds(100, 100, 510, 570);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		panel = new GraphPanel();
		panel.setBounds(0, 0, 500, 500);
		getContentPane().add(panel);
		panel.loadMap(map);

		JLabel iterationLabel = new JLabel("It:");
		iterationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		iterationLabel.setBounds(10, 512, 20, 21);
		getContentPane().add(iterationLabel);

		labelIteration = new JLabel();
		labelIteration.setBounds(42, 513, 65, 20);
		getContentPane().add(labelIteration);

		JLabel lblDistance = new JLabel("Distance:");
		lblDistance.setBounds(117, 513, 82, 18);
		getContentPane().add(lblDistance);

		labelDistance = new JLabel("");
		labelDistance.setBounds(194, 512, 70, 21);
		getContentPane().add(labelDistance);

		JLabel lblPath = new JLabel("Path:");
		lblPath.setBounds(268, 512, 46, 21);
		getContentPane().add(lblPath);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(326, 499, 164, 46);
		getContentPane().add(scrollPane);

		labelPath = new JLabel("");
		scrollPane.setViewportView(labelPath);
		this.map = map;
	}

	public void applyRoute(int iteration, Route r) {
		panel.applyPath(r);
		panel.repaint();
		labelPath.setText("" + r);
		labelDistance.setText("" + map.getDistance(r));
		labelIteration.setText("" + (iteration+1));
	}
}
