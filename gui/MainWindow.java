/**
 * Wiktor Baran
 * Marcin Lachowicz
 * Martyna Lukomska
 */

package gui;

import genetics.BaseCrossover;
import genetics.BaseMutation;
import genetics.BaseSelector;
import genetics.GeneticsEngine;
import genetics.InversionMutation;
import genetics.OrderCrossover;
import genetics.PartiallyMatchedCrossover;
import genetics.RouletteSelector;
import genetics.ScrambleMutation;
import genetics.TournamentSelector;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import map.Map;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	GeneticsEngine engine;
	Map map;

	GraphPanel panel_graph;

	JButton btnStart;
	private JTextField textField_StopCond;
	JCheckBox chckbxStopCond;
	JSpinner spinner_MaxIterations;
	JSpinner spinner_CrossoverProb;
	JSpinner spinner_MutationProb;
	JRadioButton rdbtnInversion;
	JRadioButton rdbtnPmx;
	JRadioButton rdbtnRoulette;
	JSpinner spinner_PopulationSize;
	private JRadioButton rdbtnOx;
	private JRadioButton rdbtnScramble;
	private JRadioButton rdbtnTournament;
	private JSpinner spinner_groupSize;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnStart = new JButton("Open simulation");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start();
			}
		});
		btnStart.setBounds(12, 478, 256, 25);
		contentPane.add(btnStart);

		panel_graph = new GraphPanel();
		panel_graph.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel_graph.setBounds(280, 12, 500, 500);
		contentPane.add(panel_graph);

		JPanel panel_Parametrs = new JPanel();
		panel_Parametrs.setBorder(new TitledBorder(new LineBorder(new Color(
				184, 207, 229)), "Parametrs", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_Parametrs.setBounds(12, 12, 253, 173);
		contentPane.add(panel_Parametrs);
		panel_Parametrs.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(44, 15, 0, 0);
		panel_Parametrs.add(label);

		JLabel lblMaxIterations = new JLabel("Max. iterations");
		lblMaxIterations.setBounds(12, 23, 131, 15);
		panel_Parametrs.add(lblMaxIterations);

		spinner_MaxIterations = new JSpinner();
		spinner_MaxIterations.setModel(new SpinnerNumberModel(new Integer(200),
				new Integer(1), null, new Integer(1)));
		spinner_MaxIterations.setBounds(141, 21, 100, 20);
		panel_Parametrs.add(spinner_MaxIterations);

		JLabel lblCrossoverProb = new JLabel("Crossover prob.");
		lblCrossoverProb.setBounds(12, 86, 131, 15);
		panel_Parametrs.add(lblCrossoverProb);

		JLabel lblMutationProb = new JLabel("Mutation prob.");
		lblMutationProb.setBounds(12, 113, 131, 20);
		panel_Parametrs.add(lblMutationProb);

		spinner_CrossoverProb = new JSpinner();
		spinner_CrossoverProb.setModel(new SpinnerNumberModel(0.8, 0.0, 1.0,
				0.0));
		spinner_CrossoverProb.setEditor(new JSpinner.NumberEditor(
				spinner_CrossoverProb, "0.00"));
		spinner_CrossoverProb.setBounds(141, 82, 100, 20);
		panel_Parametrs.add(spinner_CrossoverProb);

		spinner_MutationProb = new JSpinner();
		spinner_MutationProb.setModel(new SpinnerNumberModel(0.02, 0.0, 1.0,
				0.01));
		spinner_MutationProb.setEditor(new JSpinner.NumberEditor(
				spinner_MutationProb, "0.00"));
		spinner_MutationProb.setBounds(141, 114, 100, 20);
		panel_Parametrs.add(spinner_MutationProb);

		chckbxStopCond = new JCheckBox("Stop cond.");
		chckbxStopCond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxStopCond.isSelected()) {
					textField_StopCond.setEnabled(true);
				} else {
					textField_StopCond.setEnabled(false);
				}
			}
		});
		chckbxStopCond.setBounds(12, 141, 119, 23);
		panel_Parametrs.add(chckbxStopCond);

		textField_StopCond = new JTextField();
		textField_StopCond.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_StopCond.setEnabled(false);
		textField_StopCond.setBounds(141, 141, 100, 23);
		panel_Parametrs.add(textField_StopCond);
		textField_StopCond.setColumns(10);

		JLabel lblPopulationSize = new JLabel("Population size");
		lblPopulationSize.setBounds(12, 50, 110, 24);
		panel_Parametrs.add(lblPopulationSize);

		spinner_PopulationSize = new JSpinner();
		spinner_PopulationSize
				.setModel(new SpinnerNumberModel(500, 1, 1000, 1));
		spinner_PopulationSize.setBounds(140, 50, 101, 20);
		panel_Parametrs.add(spinner_PopulationSize);

		JPanel panel_CrossoverType = new JPanel();
		panel_CrossoverType.setBorder(new TitledBorder(null, "Crossover type",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_CrossoverType.setBounds(12, 297, 253, 76);
		contentPane.add(panel_CrossoverType);
		panel_CrossoverType.setLayout(null);

		ButtonGroup bg_crossover = new ButtonGroup();
		rdbtnPmx = new JRadioButton("PMX");
		rdbtnPmx.setSelected(true);
		rdbtnPmx.setBounds(8, 21, 149, 23);
		panel_CrossoverType.add(rdbtnPmx);
		bg_crossover.add(rdbtnPmx);

		rdbtnOx = new JRadioButton("OX");
		rdbtnOx.setBounds(8, 45, 149, 23);
		panel_CrossoverType.add(rdbtnOx);
		bg_crossover.add(rdbtnOx);

		JPanel panel_MutationType = new JPanel();
		panel_MutationType.setBorder(new TitledBorder(null, "Mutation type",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_MutationType.setBounds(15, 385, 253, 81);
		contentPane.add(panel_MutationType);
		panel_MutationType.setLayout(null);

		ButtonGroup bg_mutation = new ButtonGroup();
		rdbtnInversion = new JRadioButton("Inversion");
		rdbtnInversion.setSelected(true);
		rdbtnInversion.setBounds(8, 23, 149, 23);
		panel_MutationType.add(rdbtnInversion);
		bg_mutation.add(rdbtnInversion);

		rdbtnScramble = new JRadioButton("Scramble");
		rdbtnScramble.setBounds(8, 50, 149, 23);
		panel_MutationType.add(rdbtnScramble);
		bg_mutation.add(rdbtnScramble);

		JPanel panel_SelectionType = new JPanel();
		panel_SelectionType.setBorder(new TitledBorder(null, "Selection type",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_SelectionType.setBounds(12, 197, 256, 88);
		contentPane.add(panel_SelectionType);
		panel_SelectionType.setLayout(null);

		ButtonGroup bg_selection = new ButtonGroup();
		rdbtnRoulette = new JRadioButton("Roulette");
		rdbtnRoulette.setSelected(true);
		rdbtnRoulette.setBounds(8, 20, 149, 23);
		panel_SelectionType.add(rdbtnRoulette);
		bg_selection.add(rdbtnRoulette);

		rdbtnTournament = new JRadioButton("Tournament   Group size:");
		rdbtnTournament.setSelected(true);
		rdbtnTournament.setBounds(8, 41, 189, 23);
		panel_SelectionType.add(rdbtnTournament);
		bg_selection.add(rdbtnTournament);

		spinner_groupSize = new JSpinner();
		spinner_groupSize.setModel(new SpinnerNumberModel(new Integer(1),
				new Integer(1), null, new Integer(1)));
		spinner_groupSize.setBounds(197, 39, 53, 26);
		panel_SelectionType.add(spinner_groupSize);

		JLabel lblMsg = new JLabel("");
		lblMsg.setHorizontalAlignment(SwingConstants.CENTER);
		lblMsg.setBounds(278, 524, 502, 25);
		contentPane.add(lblMsg);

		JButton btnLoadMap = new JButton("Load map");
		btnLoadMap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filename = getFileName();
				if (filename != null) {
					try {
						map = loadMap(filename);
						panel_graph.loadMap(map);
					} catch (ClassNotFoundException | IOException e1) {
						JOptionPane.showMessageDialog(null,
								"Error while loading map");
					}

				} else {
					JOptionPane.showMessageDialog(null,
							"Error while loading map");
				}
			}
		});
		btnLoadMap.setBounds(12, 515, 130, 25);
		contentPane.add(btnLoadMap);

		JButton btnMapEditor = new JButton("Map editor");
		btnMapEditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MapEditor mapEditor = new MapEditor();
				mapEditor.setVisible(true);
			}
		});
		btnMapEditor.setBounds(145, 515, 123, 25);
		contentPane.add(btnMapEditor);

	}

	private void start() {
		if (map != null) {
			int stopCond = -1;
			try {
				stopCond = getStopCond();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"Error while parsing stop condition");
				return;
			}

			engine = new GeneticsEngine(getMaxIterations(), stopCond,
					getPopulationSize(), map, getSelector(), getCrossover(),
					getMutation(), getCrossoverProb(), getMutationProb());
			try {
				WorkingWindow workingWindow = new WorkingWindow(engine);
				workingWindow.setVisible(true);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(null, "Map not selected");
		}
	}

	private int getMaxIterations() {
		return (int) spinner_MaxIterations.getValue();
	}

	private int getPopulationSize() {
		return (int) spinner_PopulationSize.getValue();
	}

	private double getCrossoverProb() {
		return (double) spinner_CrossoverProb.getValue();
	}

	private double getMutationProb() {
		return (double) spinner_MutationProb.getValue();
	}

	private BaseCrossover getCrossover() {
		BaseCrossover result = null;
		if (rdbtnPmx.isSelected()) {
			result = new PartiallyMatchedCrossover();
		} else if (rdbtnOx.isSelected()) {
			result = new OrderCrossover();
		}
		return result;
	}

	private BaseMutation getMutation() {
		BaseMutation result = null;
		if (rdbtnInversion.isSelected()) {
			result = new InversionMutation();
		} else if (rdbtnScramble.isSelected()) {
			result = new ScrambleMutation();
		}
		return result;
	}

	private BaseSelector getSelector() {
		BaseSelector result = null;
		if (rdbtnRoulette.isSelected()) {
			result = new RouletteSelector();
		} else if (rdbtnTournament.isSelected()) {
			result = new TournamentSelector(
					(Integer) spinner_groupSize.getValue());

		}
		return result;
	}

	private Map loadMap(String name) throws IOException, ClassNotFoundException {
		Map result = null;
		FileInputStream fileIn = new FileInputStream(name);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		result = (Map) in.readObject();
		in.close();
		fileIn.close();
		return result;
	}

	private String getFileName() {
		String result = null;
		JFileChooser c = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Map file", "map");
		c.setFileFilter(filter);
		c.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int rVal = c.showOpenDialog(this);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			result = c.getSelectedFile().getPath();
		}
		return result;
	}

	private int getStopCond() throws Exception {
		int stopCond = -1;
		if (chckbxStopCond.isSelected()) {
			stopCond = Integer.parseInt(textField_StopCond.getText());
		}
		return stopCond;
	}
}
