package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

import map.Map;

public class MapEditor extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private GraphEditorPanel editorPanel;
	private JTextField textField_filename;

	/**
	 * Create the frame.
	 */
	public MapEditor() {
		setTitle("Map editor");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 510, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		editorPanel = new GraphEditorPanel();
		getContentPane().add(editorPanel);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 505, 480, 38);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton btnSaveMap = new JButton("Save map");
		btnSaveMap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String filename = textField_filename.getText();
				if (filename == "") {
					JOptionPane.showMessageDialog(null, "Enter map name");
				} else {
					String filePath = pickFilepath();
					if (filePath != null) {
						try {
							saveFile(filePath, filename);
							JOptionPane.showMessageDialog(null,
									"Map saved succesfully");
						} catch (IOException e) {
							JOptionPane.showMessageDialog(null,
									"Failed to save map");
						}
					}
				}
			}
		});
		btnSaveMap.setBounds(313, 11, 117, 25);
		panel.add(btnSaveMap);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(42, 13, 70, 20);
		panel.add(lblName);

		textField_filename = new JTextField();
		textField_filename.setBounds(130, 12, 171, 23);
		panel.add(textField_filename);
		textField_filename.setColumns(10);

		addMenu();
	}

	protected void saveFile(String filePath, String name) throws IOException {
		String fullPath = filePath + File.separator + name + ".map";
		FileOutputStream fileOut = new FileOutputStream(fullPath);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		Map map = editorPanel.getMap();
		map.setName(name);
		out.writeObject(map);
		out.close();
		fileOut.close();
	}

	protected String pickFilepath() {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("choosertitle");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile().getAbsolutePath();
		}
		return null;
	}

	private void addMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu modeMenu = editorPanel.getGM().getModeMenu();
		modeMenu.setText("Mouse Mode");
		modeMenu.setIcon(null); // I'm using this in a main menu
		modeMenu.setPreferredSize(new Dimension(80, 20)); // Change the size
		menuBar.add(modeMenu);
		setJMenuBar(menuBar);
	}
}
