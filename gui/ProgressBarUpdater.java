package gui;

import genetics.GeneticsEngine;

import javax.swing.JProgressBar;

public class ProgressBarUpdater extends Thread {
	private JProgressBar bar;
	private GeneticsEngine engine;

	public ProgressBarUpdater(JProgressBar bar, GeneticsEngine engine) {
		this.bar = bar;
		this.engine = engine;
		reset();
		bar.setStringPainted(true);
	}

	@Override
	public void run() {
		int currentIteration = engine.getCurrentIteration() + 1;
		int maxIterations = engine.getMaxIterations();
		while (engine.isWorking()) {
			currentIteration = engine.getCurrentIteration() + 1;
			bar.setValue(currentIteration);
			bar.setString("Iteration: " + currentIteration + "/"
					+ maxIterations);
		}
		if(!engine.isStopCondSet()) {
			bar.setValue(maxIterations);
			bar.setString("Iteration: " + maxIterations + "/"
					+ maxIterations);
		}
	}

	public void reset() {
		bar.setMaximum(0);
		bar.setMaximum(engine.getMaxIterations());
		bar.setString("");
		bar.setValue(0);
	}
}
