package gui;

import genetics.GeneticsEngine;
import genetics.IterationResult;
import genetics.Result;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import map.Route;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class WorkingWindow extends JFrame implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GeneticsEngine engine;
	int lastUpdateIndex = 0;
	ITrace2D avgTrace, minTrace, maxTrace;

	JProgressBar progressBar;
	protected Route bestRoute;
	int currentIteration;

	GraphWindow graphWindow;
	private int updateTheBestRouteInterval = 10;
	private JButton btnStart;
	private JSpinner spinner;
	private SpinnerNumberModel spinnerModel;

	public WorkingWindow(GeneticsEngine engine) {
		setTitle("Simulation");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.engine = engine;
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(null);

		Chart2D chart = new Chart2D();
		ChartPanel panel_chart = new ChartPanel(chart);
		panel_chart.setBounds(12, 89, 768, 439);
		getContentPane().add(panel_chart);

		progressBar = new JProgressBar();
		progressBar.setBounds(83, 535, 246, 28);
		getContentPane().add(progressBar);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 12, 768, 65);
		getContentPane().add(scrollPane_1);

		JTextArea label_simInfo = new JTextArea("");
		scrollPane_1.setViewportView(label_simInfo);
		label_simInfo.setEditable(false);
		label_simInfo.setLineWrap(true);

		label_simInfo.setText(getSimInfo());

		JLabel lblNewLabel = new JLabel("Iteration:");
		lblNewLabel.setBounds(347, 540, 73, 23);
		getContentPane().add(lblNewLabel);

		spinner = new JSpinner();
		spinnerModel = new SpinnerNumberModel(new Integer(0), new Integer(0),
				new Integer(0), new Integer(1));
		spinner.setModel(spinnerModel);
		spinner.setBounds(438, 540, 88, 23);
		getContentPane().add(spinner);

		JButton btnDrawTheBest = new JButton("Draw the best");
		btnDrawTheBest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IterationResult ir = WorkingWindow.this.getTheBestResult();
				graphWindow.applyRoute(ir.getIteration(),
						ir.getTheBestSpecimen());
			}
		});
		btnDrawTheBest.setFont(new Font("Dialog", Font.BOLD, 10));
		btnDrawTheBest.setBounds(667, 538, 113, 25);
		getContentPane().add(btnDrawTheBest);

		JButton btnDrawIteration = new JButton("Draw iteration");
		btnDrawIteration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = (int) spinner.getValue() - 1;
				if (i == -1) {
					JOptionPane.showConfirmDialog(null,
							"Iteration is not completed");
				} else {
					graphWindow.applyRoute(i, WorkingWindow.this.engine
							.getResult().get(i).getTheBestSpecimen());
				}
			}
		});
		btnDrawIteration.setFont(new Font("Dialog", Font.BOLD, 10));
		btnDrawIteration.setBounds(538, 538, 117, 25);
		getContentPane().add(btnDrawIteration);

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStart.setEnabled(false);
				new Thread(WorkingWindow.this.engine).start();
				new Thread(WorkingWindow.this).start();
			}
		});
		btnStart.setFont(new Font("Dialog", Font.BOLD, 9));
		btnStart.setBounds(12, 535, 64, 28);
		getContentPane().add(btnStart);

		chart.getAxisX().getAxisTitle().setTitle("Iteration");
		chart.getAxisY().getAxisTitle().setTitle("Distance");
		avgTrace = new Trace2DSimple();
		minTrace = new Trace2DSimple();
		maxTrace = new Trace2DSimple();
		chart.addTrace(avgTrace);
		chart.addTrace(minTrace);
		chart.addTrace(maxTrace);

		avgTrace.setName("Average");
		minTrace.setName("The best");
		maxTrace.setName("The worst");

		avgTrace.setColor(Color.blue);
		minTrace.setColor(Color.green);
		maxTrace.setColor(Color.red);

		graphWindow = new GraphWindow(engine.getMap());
		this.setVisible(true);
		graphWindow.setVisible(true);
	}

	@Override
	public void run() {
		ProgressBarUpdater pbUpdater = new ProgressBarUpdater(progressBar,
				engine);
		pbUpdater.start();
		while (engine.isWorking()) {
			checkAndUpdate();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
		}
		checkAndUpdate();
	}

	private IterationResult getTheBestResult() {
		Result result = engine.getResult();
		int theBestIndex = -1;
		if (result.size() > 0) {
			theBestIndex = 0;
			for (int i = 1; i < result.size(); i++) {
				if (result.get(i).getMin() < result.get(theBestIndex).getMin()) {
					theBestIndex = i;
				}
			}
		}
		if (theBestIndex == -1) {
			return null;
		}
		return result.get(theBestIndex);
	}

	private String getSimInfo() {
		String result = "Max. iterations: " + engine.getMaxIterations()
				+ ", Population size: " + engine.getPopulationSize()
				+ ", Selector type: "
				+ engine.getSelector().getClass().getSimpleName()
				+ ", Crossover type: "
				+ engine.getCrossover().getClass().getSimpleName() + " prob.: "
				+ engine.getCrossoverProbability() + ", Mutation type: "
				+ engine.getMutation().getClass().getSimpleName() + " prob.: "
				+ engine.getMutationProbability() + ", Map name: "
				+ engine.getMap().getName();

		return result;
	}

	private void checkAndUpdate() {
		spinnerModel.setValue(1);
		spinnerModel.setMinimum(1);
		currentIteration = engine.getCurrentIteration();
		if (currentIteration > lastUpdateIndex) {
			spinnerModel.setMaximum(currentIteration + 1);
			Result result = engine.getResult();
			for (int i = lastUpdateIndex; i <= currentIteration; i++) {
				IterationResult ir = result.get(i);
				avgTrace.addPoint(i, ir.getAvg());
				minTrace.addPoint(i, ir.getMin());
				maxTrace.addPoint(i, ir.getMax());
				updateTheBestRouteOnGraph(i);
			}
			lastUpdateIndex = currentIteration;
		}
	}

	private void updateTheBestRouteOnGraph(int i) {
		if ((i % updateTheBestRouteInterval) == 0) {
			IterationResult ir = engine.getResult().get(i);
			graphWindow.applyRoute(ir.getIteration(), ir.getTheBestSpecimen());
		}
	}
}
