package map;

import java.io.Serializable;
import java.util.LinkedList;

public class AdjacencyMatrix implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int[] verticesIDs;
	private int[][] matrix;

	public AdjacencyMatrix(LinkedList<Vertice> list) {
		int size = list.size();
		verticesIDs = new int[size];
		matrix = new int[size][size];

		for (int i = 0; i < size; i++) {
			Vertice currentVertice = list.get(i);

			verticesIDs[i] = currentVertice.getId();
			for (int j = 0; j < size; j++) {
				if (i == j) {
					matrix[i][j] = -1;
				} else {
					Vertice target = list.get(j);
					matrix[i][j] = (int) Math
							.sqrt(Math.pow(
									currentVertice.getX() - target.getX(), 2)
									+ Math.pow(
											currentVertice.getY()
													- target.getY(), 2));
				}
			}
		}
	}

	public int getDistance(Vertice v1, Vertice v2) {
		return getDistanceByVerticeId(v1.getId(), v2.getId());
	}

	public int getDistanceByVerticeId(int id1, int id2) {
		int i = getIndexByVerticeID(id1);
		int j = getIndexByVerticeID(id2);

		return matrix[i][j];
	}
	
	private int getIndexByVerticeID(int id) {
		int result = -1;
		for(int i=0; i<verticesIDs.length; i++) {
			if(verticesIDs[i] == id) {
				result = i;
				break;
			}
		}
		return result;
	}
}
