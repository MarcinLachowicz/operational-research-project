package map;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

public class Map implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AdjacencyMatrix distances;
	private LinkedList<Vertice> vertices;
	private String name;

	public Map(String name, LinkedList<Vertice> list) {
		this.name = name;
		vertices = list;
		distances = new AdjacencyMatrix(vertices);
	}

	public Route getRandomRoute() {
		LinkedList<Integer> verticesIDs = new LinkedList<>();
		for (Vertice v : vertices) {
			verticesIDs.add(v.getId());
		}

		int[] tempResult = new int[verticesIDs.size()];

		Random rnd = new Random();
		for (int i = 0; i < tempResult.length; i++) {
			int index = rnd.nextInt(verticesIDs.size());
			tempResult[i] = verticesIDs.get(index);
			verticesIDs.remove(index);
		}

		return new Route(tempResult);
	}

	public int getDistance(Route r) {
		int result = 0;
		for (int i = 1; i < r.getLength(); i++) {
			int id1 = r.getVerticeIDAt(i - 1);
			int id2 = r.getVerticeIDAt(i);
			if(id1 == -1) {
				System.out.println("1----");
				System.out.println(i);
				System.out.println(r);
			}
			if(id2 == -1) {
				System.out.println("2----");
				System.out.println(i);
				System.out.println(r);
			}
			result += distances.getDistanceByVerticeId(id1, id2);
		}
		result += distances.getDistanceByVerticeId(
				r.getVerticeIDAt(r.getLength() - 1), r.getVerticeIDAt(0));
		return result;
	}

	public Vertice getVerticeById(int id) {
		Vertice result = null;
		for (Vertice v : vertices) {
			if (v.getId() == id) {
				result = v;
				break;
			}
		}
		return result;
	}

	public int getSize() {
		return vertices.size();
	}

	public Vertice getVerticeAt(int i) {
		return vertices.get(i);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
