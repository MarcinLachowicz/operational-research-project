package map;

import java.util.Arrays;

public class Route {
	private int[] verticesID;
	
	public Route(int[] vID) {
		setVerticesID(vID.clone());
	}
	
	public int getLength() {
		return getVerticesID().length;
	}
	
	public int getVerticeIDAt(int i) {
		return getVerticesID()[i];
	}
	
	public int[] getSubRoute(int from, int to) {
		int[] result = new int[to-from];
		
		for(int i=0; i<result.length; i++) {
			result[i] = getVerticesID()[from+i];
		}
		
		return result;
	}
	
	public int getIndexOfVerticeID(int id) {
		for(int i=0; i<getVerticesID().length; i++) {
			if(id == getVerticesID()[i]) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean isVerticeIDBetween(int id, int from, int to) {
		int index = getIndexOfVerticeID(id);
		return (from <= index && index <= to);
	}
	
	@Override
	public String toString() {
		return Arrays.toString(getVerticesID());
	}
	
	public boolean equals(Route p2){
		super.equals(p2);
		if(Arrays.equals(getVerticesID(), p2.getSubRoute(0, p2.getLength()-1))) return true;
		return false;
	}

	public int[] getVerticesID() {
		return verticesID;
	}

	public void setVerticesID(int[] verticesID) {
		this.verticesID = verticesID;
	}
	
	
}
