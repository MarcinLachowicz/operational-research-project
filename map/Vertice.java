package map;

import java.io.Serializable;

public class Vertice implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id, x, y;

	public Vertice(int id, int x, int y) {
		this.x = x;
		this.y = y;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
